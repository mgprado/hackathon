json.array!(@donations) do |donation|
  json.extract! donation, :id, :amt, :data, :donor_id
  json.url donation_url(donation, format: :json)
end
